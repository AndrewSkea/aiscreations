# AISCreate

This is the public repository for the AISCreate company website

## Getting Started

This project aims to use GitLab CI for a seamless deployment to build and production environments.

### Prerequisites

```
A server to host the website on
docker
docker-compose
gitlab ssh access from server
```

### Local Development Environment Setup

* Create a virtual environment for python installation
* Clone this repository
* pip install the requirements.txt file
* run the following commands
```
python manage.py makemigrations --settings=aiscreate.settings.local
python manage.py migrate --settings=aiscreate.settings.local
python manage.py createsuperuser                                ## Then follow the steps
python manage.py runserver --settings=aiscreate.settings.local
```
* Have fun navigating around
* To add projects, login to the admin site (/admin) and manually add an instance to the project table
* Refresh the site

## Built With

* [Django](https://www.djangoproject.com/) - The web framework used
* [Bootstrap](https://getbootstrap.com/) - Content Organiser
* [Crispy Forms](https://django-crispy-forms.readthedocs.io/en/latest/) - Professional Form design

## Authors

* **Andrew Skea** - *Website work* - (https://gitlab.com/AndrewSkea)
