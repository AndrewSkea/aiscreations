#!/bin/sh

echo "Create database migration files"
python manage.py makemigrations  users main --no-input
python manage.py makemigrations  --no-input

echo "Apply database migrations"
python manage.py migrate --no-input

echo "Collecting static assets"
python manage.py collectstatic --noinput

echo "Starting server"
gunicorn aiscreate.wsgi -b "0.0.0.0:8000"
