from django.urls import path
from .views import HomeView, PortfolioView, AboutView

app_name = "main"

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('about/', AboutView.as_view(), name='about'),
    path('portfolio/', PortfolioView.as_view(), name='portfolio'),
]