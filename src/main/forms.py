from django import forms
from .models import Contact


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = [
            'full_name',
            'email',
            'message',
            'subject'
        ]

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['full_name'].widget = forms.TextInput(attrs={
            'placeholder': 'Name',
            'required': True})

        self.fields['email'].widget = forms.EmailInput(attrs={
            'placeholder': 'Email',
            'required': True})

        self.fields['message'].widget = forms.Textarea(attrs={
            'placeholder': 'Message',
            'rows': 6,
            'required': True})

        self.fields['subject'].widget = forms.Textarea(attrs={
            'placeholder': 'Subject',
            'rows': 6,
            'required': True})