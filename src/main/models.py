from django.db import models


class Project(models.Model):
    title = models.CharField(max_length=500)
    description = models.TextField()
    url = models.URLField(max_length=300)
    skills = models.CharField(max_length=500)
    pub_date = models.DateTimeField()
    img_name = models.CharField(max_length=200)
    image_path = models.CharField(max_length=200, blank=True, default='/')


class Contact(models.Model):
    full_name = models.CharField(max_length=100)
    email = models.EmailField(blank=False, max_length=250)
    message = models.TextField(blank=False)
    subject = models.TextField(blank=False)
