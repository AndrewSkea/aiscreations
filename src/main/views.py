from django.views.generic import View
from django.shortcuts import render
from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.http import HttpResponseRedirect

from .forms import ContactForm
from .models import Project


class HomeView(View):
    template_name = 'main/main.html'
    form_class = ContactForm

    def get(self, request, *args, **kwargs):
        form = ContactForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = ContactForm(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            try:
                send_mail(
                    str("AIS {} - {}".format(instance.full_name, instance.subject)),
                    str(instance.message),
                    str(instance.email),
                    [settings.EMAIL_TO],
                    fail_silently=True
                )
                messages.success(request, 'Your message was sent successfully. I will get back to you shortly.')
            except Exception as e:
                print(e)
                messages.error(request, 'Your message has not been sent successfully, please try again later.')
            return HttpResponseRedirect('/#contact')
        return render(request, self.template_name, {'form': form})


class AboutView(View):
    template_name = 'main/about.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class PortfolioView(View):
    template_name = 'main/portfolio.html'

    def get(self, request, *args, **kwargs):
        projects = Project.objects.all()
        for project in projects:
            project.skills = [x for x in str(project.skills).split(',')]
        return render(request, self.template_name, {'projects': projects})
